import Vue from 'vue';
import Vuex from 'vuex';

import systemModules from './modules/system.js';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
	modules: {
		systemModules
	},
	strict: debug
})
