import {
	GET_SYSTEM_INFO,
	CHANGE_GUIDE_STATUS
} from '../type.js';

function createState() {
	return {
		system: uni.getStorageSync('system').data || null,
		guide: uni.getStorageSync('guide') || false,
	};
}

export default {
	state: createState(),
	getters: {
		getSystemInfo: state => state.system,
		getGuideInfo: state => state.guide,
	},
	actions: {
		saveSysinfoToVuex: ({
			commit
		}, data) => {
			uni.setStorageSync("system", data);
			commit(GET_SYSTEM_INFO, data);
		},
		saveGuideInfoToVuex: ({
			commit
		}, data) => {
			uni.setStorageSync("guide", data);
			commit(CHANGE_GUIDE_STATUS,data);
		}
	},
	mutations: {
		[GET_SYSTEM_INFO]: (state, data) => {
			state.system = data;
		},
		[CHANGE_GUIDE_STATUS]: (state,data) => {
			state.guide = data;
		}
	}
}
