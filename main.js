import Vue from 'vue'
import App from './App'
import uView from 'uview-ui';
import httpInterceptor from '@/common/http.interceptor.js'
import tools from '@/common/plugins.tools.js';
import api from '@/common/http.api.js';
import store from '@/store/store.js';
import router from '@/common/plugin.router.js';

import '@/common/plugins.filter.js';

Vue.use(uView);

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	store,
	...App
})

Vue.use(httpInterceptor, app);
Vue.use(tools, app);
Vue.use(api, app);
Vue.use(router, app);

app.$mount()
