import Vue from 'vue'
//import _ from 'lodash' //是一个一致性、模块化、高性能的 JavaScript 实用工具库
import dayjs from 'dayjs' //日期工具

//null转空字符串
Vue.filter('FilterNullToEmpty', function (obj, propertyName) {
    if (obj) {
        return obj[propertyName];
    }
    return '';
});

//金额格式化显示
Vue.filter('FilterAmountFormat', function (number) {
    if(number) {
        number=Math.round(parseFloat(number) * 100) / 100;//保留两位小数
        let numStrings = number.toString().split('.');
        let integerFraction = numStrings[0];
        let decimalFraction='00';
        if(numStrings.length>1) {
            decimalFraction = numStrings[1] == null ? '00' : numStrings[1].padEnd(2, '0');
        }
        return `${integerFraction}.${decimalFraction}`;
    }
    return '0.00';
});


//日期格式化显示
Vue.filter('FilterDateTimeFormat', function (ts, fmt) {
    if(ts) {
        let format = fmt ? fmt : 'YYYY-MM-DD HH:mm:ss';

        let dateM = dayjs(ts);

        if(dateM){
            return dateM.format(format);
        }
    }
    return '';
});

//数字格式化显示
Vue.filter('FilterMoneyFormat', function (num,unit) {
    if(unit)
        unit='('+unit+')';
    else
        unit='';
    if(num) {
        num=Math.round(parseFloat(num) * 100) / 100;//保留两位小数
        let numStrings = num.toString().split('.');
        let integerFraction = numStrings[0];
        let decimalFraction='00';
        if(numStrings.length>1) {
            decimalFraction = numStrings[1] == null ? '00' : numStrings[1].padEnd(2, '0');
        }
        return `${integerFraction}.${decimalFraction}`+unit;
    }
    return '0.00'+unit;
});

//字符串过长截断
Vue.filter('FilterTextMaxLength', function (content, maxLen) {
    if (content) {
        let len = content.length;

        if (!maxLen) {
            maxLen = 10;
        }

        if (len > maxLen) {
            return content.substring(0, maxLen) + '...';
        }
    }
    return content;
});


//数字格式化显示,单位为万
Vue.filter('FilterWanFormat', function (num,unit) {
    if(num) {
        num = num.toFixed(2);
        num = parseFloat(num)/10000;
        num = num.toLocaleString();
        if(unit)
            unit='('+unit+')';
        else
            unit='';
        return num+unit;//返回的是字符串23,245.12保留2位小数
    }else
        return num;

});

//身份证隐藏
Vue.filter('FilterIdCard', function (code) {
    if (code) {
        let newCode = '';
        if (code.length == 18)
            newCode = code.substring(0, 10) + "***" + code.substring(16, 18);
        else if (code.length == 15)
            newCode = code.substring(0, 8) + "***" + code.substring(13, 15);
        return newCode;
    } else
        return code;

});